import supertest from 'supertest';
import urls from '../config';

const Check = {
	get: async(mail, key) => {
	  const r = await supertest(urls.mailboxlayer)
	.get(`/check?access_key=${key}&email=${mail}&smtp=1&format=1`)
	.set('Accept', 'application/json');
	return r;
	}	
};

export default Check;
