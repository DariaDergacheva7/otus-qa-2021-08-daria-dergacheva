import Check from '../framework/services/check.service';
import api from '../framework/services';
import supertest from 'supertest';
import urls from '../framework/config/index';

test('1.Проверка адреса электронной почты ', async () => { 
	const mail = 'test@mail.ru';
	const key = '73185621e9695265540c1d4923e3219c';
	const r = await api().Check().get(mail, key);
	expect(r.body.email).toEqual('test@mail.ru');
});

describe('2. Проверка валидности адреса электронной почты', () => {
test.each`
	mail													| expected
	${'simple@example.com'}									| ${true}
	${'very.common@example.com'}							| ${true}
	${'disposable.style.email.with+symbol@example.com'}		| ${true}
	${'other.email-with-hyphen@example.com'}				| ${true}
	${'fully-qualified-domain@example.com'}					| ${true}
	${'user.name+tag+sorting@example.com'}					| ${true}
	${'x@example.com'}										| ${true}
	${'example-indeed@strange-example.com'}					| ${true}
	${'test/test@test.com'}									| ${true}
	${'admin@mailserver1'}									| ${true}
	${'example@s.example'}									| ${true}
	${'" "@example.org'}									| ${true}
	${'"john..doe"@example.org'}							| ${true}
	${'mailhost!username@example.org'}						| ${true}
	${'user%example.com@example.org'}						| ${true}
	${'user-@example.org'}									| ${true}
	`('Email $mail format valid - $expected', async({mail, expected }) => {
	const key = '73185621e9695265540c1d4923e3219c';
	const r = await api().Check().get(mail, key);
	expect(r.body.format_valid).toEqual(expected);
		}); 

 test.each`
	mail																					| expected					|description
	${'Abc.example.com'}																	| ${'format_not_valid'}		|${'без символа @'}
	${'A@b@c@example.com'}																	| ${'format_not_valid'}		|${'вне кавычек допускается только один @'}
	${'a"b(c)d,e:f;g<h>i[j\k]l@example.com'}												| ${'format_not_valid'}		|${'ни один из специальных символов в этой локальной части не может быть вне кавычек'}
	${'just"not"right@example.com'}															| ${'format_not_valid'}		|${'строки в кавычках должны быть разделены точками или единственным элементом, составляющим локальную часть'}
	${'this is"not\allowed@example.com'}													| ${'format_not_valid'}		|${'пробелы, кавычки и обратная косая черта могут существовать только внутри строк в кавычках и им предшествует обратная косая черта'}
	${'this\ still\"not\\allowed@example.com'}												| ${'format_not_valid'}		|${'даже если они экранированы, пробелы, кавычки и обратные косые черты должны по-прежнему заключаться в кавычки'}
	${'QA[icon]CHOCOLATE[icon]@test.com'}													| ${'format_not_valid'}		|${'символы значков'}
	`('Email $mail - $expected = $description', async({mail, expected }) => {
	const key = '73185621e9695265540c1d4923e3219c';
	const r = await api().Check().get(mail, key);
	expect(r.body.error.type).toEqual(expected);
		});
});
			
test('3.User did not supply an Access Key - statys 101', async () => { 
	const mail = 'neboglaska@mail.ru';
	const key = '';
	const r = await api().Check().get(mail);
	expect(r.body.error.code).toEqual(101);
});
//Не валидные адреса(эти тесты падают)
//${'1234567890123456789012345678901234567890123456789012345678901234+x@example.com'}		| ${'format_not_valid'}		|${'локальная часть длиннее 64 символов'}
//${'i_like_underscore@but_its_not_allowed_in_this_part.example.com'}						| ${'format_not_valid'}		|${'Подчеркивание недопустимо в доменной части'}

